class Geom:
    name = "Geom"

    def set_coords(self, x1, y1, x2, y2):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2

    def draw(self):
        print("Draw Rect")


class Line(Geom):
    def draw(self):
        print("Draw Line")


class Rect(Geom):
    pass


l = Line()
r = Rect()
l.set_coords(1, 2, 3, 4)
r.set_coords(1, 1, 2, 2)
print(l.__dict__)
print(r.__dict__)
r.draw()
l.draw()