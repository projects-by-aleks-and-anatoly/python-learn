class Point:
    def __init__(self, x, y):
        self.__x = self.__y = 0
        if self.__check_value(x)  and self.__check_value(y):
            self.__x = x
            self.__y = y

    @classmethod
    def __check_value(cls, x):
        return type(x) in (int, float)

    def set_coords(self, x, y):
        if self.__check_value(x)  and self.__check_value(y):
            self.__x = x
            self.__y = y

    def get_coords(self):
        return self.__x, self.__y


pt = Point(1, 3)
pt.set_coords(1, 2)
print(pt.get_coords())
print(pt.__dict__)