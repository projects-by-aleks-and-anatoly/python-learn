class Clock:
    __Day = 86400

    def __init__(self, sc):
        self.check_int(sc)
        self.seconds = sc % self.__Day

    @classmethod
    def __check_int_and_clock(cls, x):
        if not isinstance(x, (int, Clock)):
            raise TypeError("Must be int digit or type Clock")
        if isinstance(x, Clock):
            cls.sec = x.seconds
        elif isinstance(x, int):
            cls.sec = x

    @classmethod
    def check_int(cls, x):
        if type(x) != int:
            raise TypeError("Must be integer digit")

    def __eq__(self, other):
        self.__check_int_and_clock(other)
        return self.seconds == self.sec

    def __lt__(self, other):
        self.__check_int_and_clock(other)
        return self.seconds < self.sec

    def __le__(self, other):
        self.__check_int_and_clock(other)
        return self.seconds <= self.sec


c1 = Clock(1000)
c2 = Clock(10300)
print(c1 == c2)  # jesli niet metoda na nierawienstwo(jesc na rawienswto), to print(c1 != c2) budiet not(c1 == c2)
print(c1 > c2)  # jesli niet metoda na bolshe(jesc na mienshe) to print(c1 > c2 ) budiet c2 < c1
print(c1 <= c2)  # jesli niet metoda na bolshe- rawno(jesc na mienshe-rawno) to print(c1 > c2 ) budiet c2 <= c1
