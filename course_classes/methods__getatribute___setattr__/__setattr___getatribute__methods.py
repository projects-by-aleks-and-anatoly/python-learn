class Point:
    MIN_COORD = 0
    MAKS_COORD = 100

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def set_coord(self, x, y):
        if self.MIN_COORD <= x <= self.MAKS_COORD:
            self.x = x
            self.y = y

    def get_coord(self):
        return (self.x, self.y)

    @classmethod
    def set_bound(cls, left):
        cls.MIN_COORD = left

    def __getattribute__(self, item):  # kogda idiot schitywanije atributa chieriez egzempliar klasa
        print("__getattribute__")
        return object.__getattribute__(self, item)

    def __setattr__(self, key, value):  # kogda idiot priswojenije kakomu libo attributu znaczienija
        print("__setattr__")
        if key == "z":
            raise AttributeError("dont use 'z'")
        else:
            object.__setattr__(self, key, value)  # ili self.__dict__[key] = value

    def __getattr__(self, item):  # srabotajet pri obraszczenii k niesuszczestwujeszczemu atributu
        print("__getattr__ :" + item)
        return False

    def __delattr__(self, item):  # srabotajet kogda udaliajetsia atribut
        print("__delattr__: " + item)
        object.__delattr__(self, item) # udaliajet atribut x
pt1 = Point(1, 2)
pt2 = Point(10, 20)
# jesli jesc __getattribute__ pri print(pt1.get_coord()) budziet TypeError: 'NoneType' object is not callable
# jesli jesc __getattribute__ pri print(pt2.get_coord()) budziet TypeError: 'NoneType' object is not callable
pt1.set_bound(4)
print(pt1.MIN_COORD)
a = pt1.x
print(a)
print(Point.__dict__)
print(pt1.__dict__)
print(pt1.l)
del pt1.x
print(pt1.__dict__)