class Clock:
    __day = 86400

    def __init__(self, seconds: int):
        self.sec = None
        self.check_int_(seconds)
        self.sc = seconds % self.__day

    @classmethod
    def check_int_(cls, i):
        if type(i) != int:
            raise TypeError("Must be integer digit")

    def get_time(self):
        s = self.sc % 60
        m = (self.sc // 60) % 60
        h = (self.sc // 3600)
        return f"{self.__get_formated(h)}:{self.__get_formated(m)}:{self.__get_formated(s)}"

    def __check_int_or_clock(self, x):
        if not isinstance(x, (int, Clock)):
            raise ArithmeticError("Must be integer digit or type Clock ")
        self.sec = x
        if isinstance(x, Clock):
            self.sec = x.sc

    @classmethod
    def __get_formated(cls, x):
        return str(x).rjust(2, "0")

    def __add__(self, other):
        self.__check_int_or_clock(other)
        return Clock(self.sc + self.sec)

    def __radd__(self, other):
        return self + other

    def __iadd__(self, other):
        self.__check_int_or_clock(other)
        self.sc += self.sec
        return self

    def __sub__(self, other):
        self.__check_int_or_clock(other)
        return Clock(self.sc - self.sec)

    def __rsub__(self, other):
        return self - other

    def __isub__(self, other):
        self.__check_int_or_clock(other)
        self.sc -= self.sec
        return self


p = Clock(1000)
p2 = Clock(2000)
# p.sc = p.sc + 100   # bez __add__
p3 = p + p2 - 1000  # s __add__
p += 100
print(p.get_time())
print(p3.get_time())
