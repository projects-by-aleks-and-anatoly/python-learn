class Person:
    def __init__(self, name, old):
        self.__name = name
        self.__old = old

    # def get_old(self):
    #   return self.__old

    # def set_old(self, old):
    # self.__old = old

    # old = property(get_old, set_old)

    @property
    def name(self):
        return self.__name

    @name.setter
    def name(self, name):
        self.__name = name



p1 = Person("sergej", 35)
print(p1.__dict__)
print(p1.name)