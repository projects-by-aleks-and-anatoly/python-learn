#  ex1 Convert two lists into a dictionary .Below are the two lists. Write a Python program to convert them into a dictionary in a way that item from list1 is the key and item from list2 is the value
def do_dictionary_from_two_lists(key, value):
    my_dict1 = {}
    for i in range(len(key)):
        my_dict1[key[i]] = value[i]
    print("ex1:", my_dict1)


keys = ['Ten', 'Twenty', 'Thirty']
values = [10, 20, 30]
do_dictionary_from_two_lists(keys, values)


# ex2 Merge two Python dictionaries into one.
def do_one_dictionary_from_two_dictionary(dict_1, dict_2):
    dict_1.update(dict_2)
    print("ex2:", dict_1)


dict1 = {'Ten': 10, 'Twenty': 20, 'Thirty': 30}
dict2 = {'Thirty': 30, 'Fourty': 40, 'Fifty': 50}
do_one_dictionary_from_two_dictionary(dict1, dict2)


# ex3 Print the value of key ‘history’ from the below dict
def print_value_of_key_history(sample_dict):
    x = sample_dict["class"]["student"]['marks']["history"]
    print("ex3:", x)


sampleDict = {"class": {"student": {"name": "Mike", "marks": {"physics": 70, "history": 80}}}}
print_value_of_key_history(sampleDict)


# ex4 In Python, we can initialize the keys with the same values.  Initialize dictionary with default values.
def do_one_dictionary_from_list_and_dictionary(list, dict):
    my_dictionary = {}
    my_dictionary = dict.fromkeys(list, dict)
    print("ex4:", my_dictionary)


employees = ['Kelly', 'Emma']
defaults = {"designation": 'Developer', "salary": 8000}
do_one_dictionary_from_list_and_dictionary(employees, defaults)


#  ex5 Write a Python program to create a new dictionary by extracting the mentioned keys from the below dictionary.
def print_key_values_from_dictionary(dict, list):
    new_dict = {}
    for i in range(len(list)):
        new_dict[list[i]] = dict[list[i]]
    print("ex5:", new_dict)


sample_dict = {"name": "Kelly", "age": 25, "salary": 8000, "city": "New york"}
keys = ["name", "salary", "age"]
print_key_values_from_dictionary(sample_dict, keys)