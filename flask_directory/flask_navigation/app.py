from flask import Flask, render_template, url_for

app = Flask(__name__)


# url_for("index") -----> /home
# url_for("matrix") -----> /matrix
@app.route("/")
@app.route("/home")
def index():
    return render_template("index.html")


@app.route("/matrix")
def matrix():
    return render_template("matrix.html")


if __name__ == "__main__":
    app.run()
