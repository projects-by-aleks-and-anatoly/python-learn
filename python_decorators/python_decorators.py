# Evclid Algoritm: poisk naibolshego obshchego dielitiela.
import time
from _ctypes_test import func
from math import *


def found_divider(a, b):
    if a < b:
        a, b = b, a
    while b > 0:
        a, b = b, a % b
    return a


def test_divider(func):
    # -- test 1 ----------
    a = 28
    b = 35
    nod = func(a, b)
    if nod == 7:
        print("test 1 -- ok")
    else:
        print("test 1 -- error")
    # -- test 2 --------
    a = 100
    b = 1
    nod = func(a, b)
    if nod == 1:
        print("test 2 -- ok")
    else:
        print("test 2 -- error")
    # -- test 2 --------
    a = 2
    b = 10000000000
    start = time.time()
    nod = func(a, b)
    end = time.time()
    tm = end - start
    if nod == 2 and tm < 0.03:
        print("test 3 -- ok")
        print(tm)
    else:
        print("test 3 -- error")
        print(tm)


print(found_divider(28, 32))
test_divider(found_divider)


# zamykanije w python
def say_name(name):
    def say_goodbuy():
        print("Goodbuy", name, "!")

    say_goodbuy()


say_name("Alex")


def say_name(name):
    def say_goodbuy():
        print("Goodbuy", name, "!")

    return say_goodbuy


f = say_name("Alex")
f2 = say_name("Vasia")
f()
f2()


def strip_string(strip_chars=" "):
    def do_strip(string):
        return string.strip(strip_chars)

    return do_strip


st = strip_string("!  ")
print(st("Hello. !"))


# wwiedienije w dekoratory

def decorator_func(func):
    def wrapper(*args, **kwargs):
        print("Chtoto do wyzowa")
        func(*args, **kwargs)
        print("Chtoto posle wyzowa")
        return func(*args, **kwargs)

    return wrapper


def some_func(title, tag):
    print("wyzow some func")
    print(title)
    return tag


some_func = decorator_func(some_func)
some_func("Python", "Python2")


def test_time(func):
    def wrapper2(*args, **kwargs):
        st = time.time()
        nod = func(*args, **kwargs)
        et = time.time()
        dt = et - st
        print(f"Work time of function {dt} sec")
        return "nod:", nod

    return wrapper2


@test_time
def evclid_algoritm(a, b):
    while a != b:
        if a > b:
            a = a - b
        elif b > a:
            b = b - a
    return a


@test_time
def fast_evclid_algoritm(a, b):
    if a < b:
        a, b = b, a
    while b > 0:
        a, b = b, a % b
    return a


# evclid_algoritm = test_time(evclid_algoritm)
print(evclid_algoritm(32, 12432))
# fast_evclid_algoritm = test_time(fast_evclid_algoritm)
print(fast_evclid_algoritm(2, 2))


# dekoratory z paramietrami
def parametrs_of_decorator(dx=0.01):
    def func_decorator(func):
        def wrapper3(x, *args, **kwargs):
            res = (func(x + dx, *args, **kwargs) - func(x, *args, **kwargs)) / dx
            return res

        wrapper3.__name__ = func.__name__
        return wrapper3

    return func_decorator


@parametrs_of_decorator(0.00001)
def sin_df(x):
    return sin(x)


print(sin_df(pi / 6))
print(sin_df.__name__)  # jesli nie propisat stroku wrapper3.__name__ = func.__name__ to imia sin_df budiet wrapper3


# mozno nie propisywac stroku wrapper3.__name__ = func.__name__  sdielac from functools import wraps i diekorirowac
# funcjiu wrap s pomoszczju @wraps(func)

def func_name(name=""):
    def calculate_time(func):
        def calculate(*args, **kwargs):
            start = time.time()
            func(*args, **kwargs)
            end = time.time()
            time_work = end - start
            print("func name:", name)
            print("time work:", time_work)

        return calculate

    return calculate_time


@func_name("Huj")
def quadratic_equation(a, b, c):
    delta = b ** 2 - 4 * a * c
    if delta < 0:
        print("No elements of the equation")
    elif delta == 0:
        x = -b / (2 * a)
        print('x =', x)
    else:
        x1 = (-b + delta ** 0.5) / (2 * a)
        x2 = (-b - delta ** 0.5) / (2 * a)
        print("x1 =", x1)
        print("x2 =", x2)


quadratic_equation(3, 23, -124)


def my_property(func_getter):
    def my_wrapper_getter(*args, **kwargs):
        func_getter(*args, **kwargs)
        print(*args, **kwargs)

    return my_wrapper_getter


@my_property
def old_getter(old):
    return old


old_getter(15)
